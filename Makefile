REPL ?= browser

.DEFAULT_GOAL := repl

.PHONY: clean
clean:
		rm -rf out .cpcache dist

# NOTE (BNB): I just stored the type of repl in the `$(REPL)` variable. If you
#             call `REPL=node make repl` this Makefile will use node instead of
#             your browser.
.PHONY: repl
repl:
		clj --main cljs.main --compile hello-world.core --repl --repl-env $(REPL)

.PHONY: build
build:
		clj --main cljs.main --compile hello-world.core --optimizations advanced

.PHONY: serve
serve: build
		clj --main cljs.main --serve

# NOTE (BNB): I could have put this in a package.json file managed by node. Try
#             `npm init && npm install --save-dev source-map-support`. That
#             should initialize the package.json file, and from then on you'd
#             be able to run `npm install` to get all the node dependencies.
#             Might come in handy if you need to link against some node code.
.PHONY: npm
npm:
		npm install source-map-support

# NOTE (BNB): The order of the command line arguments matters. If the
#             `--compile` comes before the `--output-to`, clj doesn't output
#             anything.
.PHONY: node
node: npm
		clj --main cljs.main --target node --output-to dist/main.js --compile hello-world.core
